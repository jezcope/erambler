;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((web-mode . ((web-mode-css-indent-offset . 2)
	      (web-mode-code-indent-offset . 2)
	      (web-mode-markup-indent-offset . 2)))
 ("themes/mnemosyne" . ((web-mode-engines-alist . '(("go" . "\\.html\\'"))))))
