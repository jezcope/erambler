# [[id:aeaf7659-4484-493c-9da4-f4c8e283404d][Build image:1]]
FROM node:lts-alpine
ENV NODE_ENV=production
RUN apk add --no-cache hugo \
    && npm install -g netlify-cli\
    && npm cache clean --force
RUN wget -P /tmp https://github.com/sass/dart-sass/releases/download/1.77.2/dart-sass-1.77.2-linux-x64-musl.tar.gz \
    && tar -C /usr/local/bin --strip-components=1 \
         -xf /tmp/dart-sass-1.77.2-linux-x64-musl.tar.gz \
    && rm /tmp/dart-sass-1.77.2-linux-x64-musl.tar.gz
# Build image:1 ends here
