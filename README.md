This is my blog. You can visit the real thing at <http://erambler.co.uk/>, or learn from the code if you wish. It's built using [Hugo](https://gohugo.io), a static site generator written in Go.

Fediverse responses to individual posts are shown thanks to [brid.gy][] and [webmention.io][].

[brid.gy]: https://brid.gy/mastodon/@petrichor@digipres.club

[webmention.io]: https://brid.gy/mastodon/@petrichor@digipres.club
