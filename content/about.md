---
title: "About me"
slug: about
menu:
    main:
        name: about
        weight: 10
---

I help researchers communicate and collaborate more effectively using technology, mainly focusing on research data management policy, practice, training and advocacy.  I currently work at [The British Library][BL] as Data Services Lead.

In my free time, I like to:
- Run, hike, climb
- Play and run [tabletop roleplaying games](https://en.wikipedia.org/wiki/Tabletop_role-playing_game) and board games (including [Go](http://en.wikipedia.org/wiki/Go_(game)))
- Play the accordion, dance British & European social dances, [morris dance][]
- Cook, write, read (fiction and non-fiction, mostly scifi & fantasy)

[Morris dance]: http://www.fiveriversmorris.org.uk/

[BL]: https://bl.uk

## Professional service & recognition

- Committee Member (Website), [Neurodivergent Library and Information Staff Network (NLISN)](https://nlisn.org/), _2024 – present_
- Editorial Board Member, [Journal of Open Humanities Data (JOHD)](https://openhumanitiesdata.metajnl.com/about/editorialteam#editorial-board), _2023 – present_
- Member and former chair, [EMEA Expert Group](https://datacite.org/regional-expert-groups/#EMEA_EG), _2021 – present (chair until 2023)_
- Fellow, [Sustainable Software Institute](https://www.software.ac.uk/programmes/fellowship-programme), _2020 inauguration ([see also my Fellow profile](https://www.software.ac.uk/fellowship-programme/jez-cope))_
- Member, [DataCite Community Engagement Steering Group](https://datacite.org/cesg/), _2018 – present_
- LIBER Award for Library Innovation (shared), for: Chue Hong, N. et al. (2021) “Recognising the value of software: how libraries can help the adoption of software citation,” in Liber annual conference 2021. LIBER. Available at: https://doi.org/10.6084/m9.figshare.14825268.
- Open Life Science programme Mentor & Expert, _2020 – present_

### Formerly

<!-- - Member of the [University Research Data Management Steering Group](http://www.sheffield.ac.uk/library/rdm/steeringgroup) -->
<!-- - Co-opted member of the [University Research Ethics Committee](http://www.sheffield.ac.uk/ris/other/committees/ethicscommittee) -->
- Member, Physical Sciences Data Science Services (PSDS) Management Advisory Panel, _2019 – 2023_
- SSI Fellowship mentor, _2019_
- Member, Digital Preservation Coalition (DPC) Workforce Development Subcommittee, _2018 – 2023_
- Member, Library Carpentry Governance Group, _2017 – 2019_
- Maintainer of the [Library Carpentry lesson about git](https://github.com/data-lessons/library-git)
- Member of the [Jisc UK Research Data Shared Service](https://jisc.ac.uk/rd/projects/research-data-shared-service) Expert Advisory Group
