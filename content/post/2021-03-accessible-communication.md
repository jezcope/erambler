---
title: "Ideas for Accessible Communications"
description: |
    A summary of my response to my workplace's survey
    on making communications more accessible.
slug: accessible-communication
date: 2021-03-20T15:43:47+00:00
tags:
- Stuff
- Accessibility
- Ablism
---

The Disability Support Network at work
recently ran a survey on "accessible communications",
to develop guidance on how to make communications
(especially internal staff comms)
more accessible to everyone.
I grabbed a copy of my submission
because I thought it would be useful to share more widely,
so here it is.
Please note that these are based on my own experiences only.
I am in no way suggesting
that these are the only things you would need to do
to ensure your communications are fully accessible.
They're just some things to keep in mind.

- - - - -

Policies/procedures/guidance can be stressful to use
if anything is vague or inconsistent,
or if it looks like there might be more information implied
than is explicitly given
(a common cause of this is use of jargon in e.g. HR policies).
Emails relating to these policies have similar problems,
made worse because they tend to be very brief.

Online meetings can be very helpful,
but can also be exhausting,
especially if there are too many people,
or not enough structure.
Larger meetings & webinars without agendas
(or where the agenda is ignored,
or timings are allowed to drift without acknowledgement)
are very stressful,
as are those where there is not enough structure
to ensure fair opportunities to contribute.

Written reference documents and communications should:

- Be carefully checked for consistency and clarity
- Have all all key points explicitly stated
- Explicitly acknowledge the need for flexibility where it is necessary,
  rather than implying or hinting at it
- Clearly define jargon & acronyms
  where they are necessary to the point being made,
  and avoid them otherwise
- Include links to longer, more explicit versions where space is tight
- Provide clear bullet-point summaries with links to the details

Online meetings should:

- Include sufficient break time
  (at least 10 minutes out of every hour)
  and not allow this to be compromised
  just because a speaker has misjudged the length of their talk
- Include initial "settling-in" time in agendas
  to avoid timing getting messed up from the start
- Ensure the agenda is stuck to,
  or that divergence from the agenda
  is acknowledged explicitly by the chair
  and updated timing briefly discussed to ensure everyone is clear
- Establish a norm for participation at the start of the meeting
  and stick to it
  e.g. ask people to raise hands when they have a point to make,
  or have specific time for round-robin contributions
- Ensure quiet/introverted people have space to contribute,
  but don't force them to do so
  if they have nothing to add at the time
- Offer a text-based alternative to contributing verbally
- If appropriate,
  at the start of the meeting
  assign specific roles of:
  - Gatekeeper: ensures everyone has a chance to contribute
  - Timekeeper: ensures meeting runs to time
  - Scribe: ensures a consistent record of the meeting
- Be chaired by someone with the confidence to enforce the above:
  offer training to all staff on chairing meetings
  to ensure everyone has the skills
  to run a meeting effectively


