---
title: "Collaborations Workshop 2021: talks & panel session"
slug: collabw21-part-1
date: 2021-04-05T21:56:09+01:00
tags:
- Technology
- Conference
- SSI
- CW21
- Collaborations Workshop
- Research
- Disability
- Equality, diversity & inclusion
series: collabw21
showTOC: true
---

I've just finished attending (online) the three days of this year's [SSI Collaborations Workshop][CW21] (CW for short), and once again it's been a brilliant experience, as well as mentally exhausting, so I thought I'd better get a summary down while it's still fresh it my mind.

[CW21]: https://software.ac.uk/cw21

Collaborations Workshop is, as the name suggests, much more focused on facilitating collaborations than a typical conference, and has settled into a structure that starts off with with longer keynotes and lectures, and progressively gets more interactive culminating with a [hack day][] on the third day.

[hack day]: https://en.wikipedia.org/wiki/Hackathon

That's a lot to write about, so for this post I'll focus on the talks and panel session, and follow up with another post about the collaborative bits. I'll also probably need to come back and add in more links to bits and pieces once slides and the "official" summary of the event become available.

{{< admonition updates >}}
**2021-04-07** Added links to recordings of keynotes and panel sessions
{{< /admonition >}}

## Provocations

The first day began with two keynotes on this year's main themes: FAIR Research Software and Diversity & Inclusion, and day 2 had a great panel session focused on disability. All three were streamed live and the recordings remain available on Youtube:

- [View the **keynotes** recording](https://www.youtube.com/watch?v=8viA4y1pz_8); _[Google-free alternative link](https://invidious.xyz/watch?v=8viA4y1pz_8)_
- [View the **panel session** recording](https://www.youtube.com/watch?v=65a8c06VHOY); _[Google-free alternative link](https://invidious.xyz/watch?v=65a8c06VHOY)_

### FAIR Research Software

Dr Michelle Barker, Director of the [Research Software Alliance][ReSA], spoke on the challenges to recognition of software as part of the scholarly record: software is not often cited. The [FAIR4RS working group][] has been set up to investigate and create guidance on how the [FAIR Principles][] for data can be adapted to research software as well; as they stand, the Principles are not ideally suited to software. This work will only be the beginning though, as we will also need metrics, training, career paths and much more. ReSA itself has 3 focus areas: people, policy and infrastructure. If you're interested in getting more involved in this, you can join the  [ReSA email list](https://groups.google.com/g/research-software-alliance).

[ReSA]: https://www.researchsoft.org/
[FAIR4RS working group]: https://www.rd-alliance.org/groups/fair-4-research-software-fair4rs-wg
[FAIR Principles]: https://www.force11.org/fairprinciples

### Equality, Diversity & Inclusion: how to go about it

Dr Chonnettia Jones, Vice President of Research, [Michael Smith Foundation for Health Research][MSFHR] spoke extensively and persuasively on the need for Equality, Diversity & Inclusion (EDI) initiatives within research, as there is abundant robust evidence that **all** research outcomes are improved.

[MSFHR]: https://www.msfhr.org/

She highlighted the difficulties current approaches to EDI have effecting structural change, and changing not just individual behaviours but the cultures & practices that perpetuate iniquity. What initiatives are often
constructed around making up for individual deficits, a bitter framing is to start from an understanding of individuals having equal stature but having different tired experiences. Commenting on the current focus on "research excellent" she pointed out that the hyper-competition this promotes is deeply unhealthy. suggesting instead that true excellence requires diversity, and we should focus on an inclusive excellence driven by inclusive leadership.

### Equality, Diversity & Inclusion: disability issues

Day 2's EDI panel session brought together five disabled academics to discuss the problems of disability in research.

- Dr Becca Wilson, UKRI Innovation Fellow, Institute of Population Health Science, University of Liverpool (Chair)
- Phoenix C S Andrews (PhD Student, Information Studies, University of Sheffield and Freelance Writer)
- Dr Ella Gale (Research Associate and Machine Learning Subject Specialist, School of Chemistry, University of Bristol)
- Prof Robert Stevens (Professor and Head of Department of Computer Science, University of Manchester)
- Dr Robin Wilson (Freelance Data Scientist and SSI Fellow)

_NB. The discussion flowed quite freely so the following summary, so the following summary mixes up input from all the panel members._

Researchers are often assumed to be single-minded in following their research calling, and aptness for jobs is often partly judged on "time send", which disadvantages any disabled person who has been forced to take a career break. On top of this disabled people are often time-poor because of the extra time needed to manage their condition, leaving them with less "output" to show for their time served on many common metrics. This can partially affect early-career researchers, since resources for these are often restricted on a "years-since-PhD" criterion. Time poverty also makes funding with short deadlines that much harder to apply for. Employers add more demands right from the start: new starters are typically expected to complete a health and safety form, generally a brief affair that will suddenly become an 80-page bureaucratic nightmare if you tick the box declaring a disability.

Many employers claim to be inclusive yet utterly fail to understand the needs of their disabled staff. Wheelchairs are liberating for those who use them (despite the awful but common phrase "wheelchair-bound") and yet employers will refuse to insure a wheelchair while travelling for work, classifying it as a "high value personal item" that the owner would take the same responsibility for as an expensive camera. Computers open up the world for blind people in a way that was never possible without them, but it's not unusual for mandatory training to be inaccessible to screen readers. Some of these barriers can be overcome, but doing so takes yet more time that could and should be spent on more important work.

What can we do about it? Academia works on patronage whether we like it or not, so be the person who supports people who are different to you rather than focusing on the one you "recognise yourself in" to mentor. As a manager, it's important to ask each individual what they need and believe them: they are the expert in their own condition and their lived experience of it. Don't assume that because someone else in your organisation with the same disability needs one set of accommodations, it's invalid for your staff member to require something totally different. And remember: disability is unusual as a protected characteristic in that anyone can acquire it at any time without warning!


## Lightning talks

Lightning talk sessions are always tricky to summarise, and while this doesn't do them justice, here are a few highlights from my notes. 

### Data & metadata

- Malin Sandstrom talked about a [much-needed refinement of contributor role taxonomies for scientific computing](https://doi.org/10.6084/m9.figshare.14331242.v1)
- Stephan Druskat showcased a [project to crowdsource a corpus of research software for further analysis](https://doi.org/10.6084/m9.figshare.14330426.v2)

### Learning & teaching/community

- Matthew Bluteau introduced the concept of the ["coding dojo" as a way to enhance community of practice](https://doi.org/10.6084/m9.figshare.14330822.v1). A group of coders got together to practice & learn by working together to solve a problem and explaining their work as they go
  - He described 2 models: a code jam, where people work in small groups, and the [Randori](https://en.wikipedia.org/wiki/Randori) method, where 2 people do pair programming while the rest observe. I'm excited to try this out!
- Steve Crouch talked about [intermediate skills and helping people take the next step](https://doi.org/10.6084/m9.figshare.14318477.v1), which I'm also very interested in with the [GLAM Data Science network](https://glamdatasci.network)
- Esther Plomp recounted experience of [running multiple Carpentry workshops online](https://doi.org/10.6084/m9.figshare.14330420.v1), while Diego Alonso Alvarez discussed [planned workshops on making research software more usable with GUIs](https://doi.org/10.6084/m9.figshare.14315966.v1)
- Shoaib Sufi [showcased](https://doi.org/10.6084/m9.figshare.14307833.v1) the [SSI's new event organising guide](https://event-organisation-guide.readthedocs.io/en/latest/index.html)
- Caroline Jay reported on a [diary study into autonomy & agency in RSE during COVID](https://doi.org/10.6084/m9.figshare.14331242.v1)
  - [Lopez, T., Jay, C., Wermelinger, M., & Sharp, H. (2021). How has the covid-19 pandemic affected working conditions for research software engineers? Unpublished manuscript.](https://doi.org/10.48420/14330807.V1)

## Wrapping up

That's not everything! But this post is getting pretty long so I'll wrap up for now. I'll try to follow up soon with a summary of the "collaborative" part of Collaborations Workshop: the idea-generating sessions and hackday!
