---
title: "Collaborations Workshop 2021: collaborative ideas & hackday"
slug: collabw21-part-2
date: 2021-04-07T16:24:10+01:00
tags:
- Technology
- Conference
- SSI
- CW21
- Collaborations Workshop
- Research
- Disability
- Equality, diversity & inclusion
series: collabw21
---

[My last post covered the more "traditional" lectures-and-panel-sessions approach]({{< ref "2021-04-collabw21-part-1" >}}) of the first half of the [SSI Collaborations Workshop][CW21]. The rest of the workshop was much more interactive, consisting of a [discussion session][], a [Collaborative Ideas session][], and a [whole-day hackathon][hackday]!

The discussion session on day one had us choose a topic (from a list of topics proposed leading up to the workshop) and join a breakout room for that topic with the aim of producing a "speed blog" by then end of 90 minutes. Those speed blogs will be published on the [SSI blog][] over the coming weeks, so I won't go into that in more detail.

The Collaborative Ideas session is a way of generating hackday ideas, by putting people together at random into small groups to each raise a topic of interest to them before discussing and coming up with a combined idea for a hackday project. Because of the serendipitous nature of the groupings, it's a really good way of generating new ideas from unexpected combinations of individual interests.

After that, all the ideas from the session, along with a few others proposed by various participants, were pitched as ideas for the hackday and people started to form teams. Not every idea pitched gets worked on during the hackday, but in the end 9 teams of roughly equal size formed to spend the third day working together.

[CW21]: https://software.ac.uk/cw21
[Collaborative Ideas session]: https://software.ac.uk/cw21/collaborative-ideas-session
[hackday]: https://software.ac.uk/cw21/hack-day
[discussion session]: https://software.ac.uk/cw21/discussion-session
[SSI blog]: https://software.ac.uk/blog

## My team's project: "AHA! An Arts & Humanities Adventure"

There's a lot of [FOMO][] around choosing which team to join for an event like this: there were so many good ideas and I wanted to work on several of them! In the end I settled on a team developing an escape room concept to help Arts & Humanities scholars understand the benefits of working with research software engineers for their research.

Five of us rapidly mapped out an example storyline for an escape room, got a website set up with GitHub and populated it with the first few stages of the game. We decided to focus on a story that would help the reader get to grips with what an [API][] is and I'm amazed how much we managed to get done in less than a day's work!

You can [try playing through the escape room (so far) yourself on the web][website], or [take a look at the GitHub repository][GitHub], which contains the source of the website along with [a list of outstanding tasks to work on][issues] if you're interested in contributing.

I'm not sure yet whether this project has enough momentum to keep going, but it was a really valuable way both of getting to know and building trust with some new people and demonstrating the concept is worth more work.

[FOMO]: https://en.wikipedia.org/wiki/Fear_of_missing_out
[API]: https://en.wikipedia.org/wiki/API
[website]: https://lostrses.github.io/escape-room/
[GitHub]: https://github.com/lostRSEs/escape-room
[issues]: https://github.com/lostRSEs/escape-room/issues

## Other projects

Here's a brief rundown of the other projects worked on by teams on the day.

Coding Confessions
: Everyone starts somewhere and everyone cuts corners from time to time. Real developers copy and paste! Fight imposter syndrome by looking through some of these confessions or contributing your own. https://coding-confessions.github.io/

CarpenPI
: A template to set up a Raspberry Pi with everything you need to run a Carpentries (https://carpentries.org/) data science/software engineering workshop in a remote location without internet access. https://github.com/CarpenPi/docs/wiki

Research Dugnads
: A guide to running an event that is a coming together of a research group or team to share knowledge, pass on skills, tidy and review code, among other software and working best practices (based on the [Norwegian concept of a dugnad](https://en.wikipedia.org/wiki/Communal_work#Norway), a form of "voluntary work done together with other people") https://research-dugnads.github.io/dugnads-hq/

Collaborations Workshop ideas
: A meta-project to collect together pitches and ideas from previous Collaborations Workshop conferences and hackdays, to analyse patterns and revisit ideas whose time might now have come. https://github.com/robintw/CW-ideas

howDescribedIs
: Integrate existing tools to improve the machine-readable metadata attached to open research projects by integrating projects like SOMEF, codemeta.json and HowFAIRIs (https://howfairis.readthedocs.io/en/latest/index.html). Complete with CI and badges! https://github.com/KnowledgeCaptureAndDiscovery/somef-github-action

Software end-of-project plans
: Develop a template to plan and communicate what will happen when the fixed-term project funding for your research software ends. Will maintenance continue? When will the project sunset? Who owns the IP? https://github.com/elichad/software-twilight

Habeas Corpus
: A corpus of machine readable data about software used in COVID-19 related research, based on the CORD19 dataset. https://github.com/softwaresaved/habeas-corpus

Credit-all
: Extend the all-contributors GitHub bot (https://allcontributors.org/) to include rich information about research project contributions such as the CASRAI Contributor Roles Taxonomy (https://casrai.org/credit/) https://github.com/dokempf/credit-all

I'm excited to see so many metadata-related projects! I plan to take a closer look at what the Habeas Corpus, Credit-all and howDescribedIs teams did when I get time. I also really want to try running a dugnad with my team or for the [GLAM Data Science network](https://glamdatasci.network).
