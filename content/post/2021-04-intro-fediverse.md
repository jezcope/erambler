---
title: Intro to the fediverse
date: 2021-04-11T20:25:45+01:00
tags:
- Fediverse
- Social media
- Twitter
---

Wow, it turns out to be 10 years since I wrote [this beginners guide to Twitter](/blog/beginners-guide-to-twitter-part-i/). Things have moved on a *loooooong* way since then.

Far from being the interesting, disruptive technology it was back then, Twitter has become part of the mainstream, the establishment. Almost everyone and everything is on Twitter now, which has both pros and cons.

# So what's the problem?

It's now possible to follow all sorts of useful information feeds, from live updates on transport delays to your favourite sports team's play-by-play performance to an almost infinite number of cat pictures.  In my professional life it's almost guaranteed that anyone I meet will be on Twitter, meaning that I can contact them to follow up at a later date without having to exchange contact details (and they have options to block me if they don't like that).

On the other hand, a medium where everyone's opinion is equally valid regardless of knowledge or life experience has turned some parts of the internet into a toxic swamp of hatred and vitriol. It's easier than ever to forget that we have more common ground with any random stranger than we have similarities, and that's led to some truly awful acts and a poisonous political arena.

Part of the problem here is that each of the social media platforms is controlled by a single entity with almost no accountability to anyone other than shareholders. Technological change has been so rapid that the regulatory regime has no idea how to handle them, leaving them largely free to operate how they want. This has led to a whole heap of nasty consequences that many other people have done a much better job of documenting than I could (Shoshana Zuboff's book [The Age of Surveillance Capitalism](https://en.wikipedia.org/wiki/The_Age_of_Surveillance_Capitalism) is a good example). What I'm going to focus on instead are some possible alternatives.

If you accept the above argument, one obvious solution is to break up the effective monopoly enjoyed by Facebook, Twitter *et al*. We need to be able to retain the wonderful affordances of social media but democratise control of it, so that it can never be dominated by a small number of overly powerful players.

# What's the solution?

There's actually a thing that already exists, that almost everyone is familiar with and that already works like this.

It's email.

There are a hundred thousand email servers, but my email can always find your inbox if I know your address because that address identifies both you and the email service you use, and they communicate using the same protocol, [Simple Mail Transfer Protocol (SMTP)](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol)[^1].  I can't send a message to your Twitter from my Facebook though, because they're completely incompatible, like oil and water. Facebook has no idea how to talk to Twitter and vice versa (and the companies that control them have zero interest in such interoperability anyway).

Just like email, a *federated* social media service like [Mastodon](https://joinmastodon.org) allows you to use any compatible server, or even run your own, and follow accounts on your home server or anywhere else, even servers running *different software* as long as they use the same [ActivityPub](http://activitypub.rocks) protocol.

There's no lock-in because you can move to another server any time you like, and interact with all the same people from your new home, just like changing your email address. Smaller servers mean that no one server ends up with enough power to take over and control everything, as the social media giants do with their own platforms. But at the same time, a small server with a small moderator team can enforce local policy much more easily and block accounts or whole servers that host trolls, nazis or other poisonous people.

# How do I try it?

I have no problem with anyone for choosing to continue to use what we're already calling "traditional" social media; frankly, Facebook and Twitter are still useful for me to keep in touch with a lot of my friends. However, I do think it's useful to know some of the alternatives if only to make a more informed decision to stick with your current choices. Most of these services only ask for an email address when you sign up and use of your real name vs a pseudonym is entirely optional so there's not really any risk in signing up and giving one a try. That said, make sure you take sensible precautions like not reusing a password from another account.

| Instead of…                      | Try…                                                                                                      |
|----------------------------------|-----------------------------------------------------------------------------------------------------------|
| Twitter, Facebook                | [Mastodon](https://joinmastodon.org/), [Pleroma](https://pleroma.social/), [Misskey](https://misskey.io/) |
| Slack, Discord, IRC              | [Matrix](https://matrix.org/)                                                                             |
| WhatsApp, FB Messenger, Telegram | Also [Matrix](https://matrix.org/)                                                                        |
| Instagram, Flickr                | [PixelFed](https://pixelfed.org/)                                                                         |
| YouTube                          | [PeerTube](https://joinpeertube.org/)                                                                     |
| The web                          | [Interplanetary File System (IPFS)](https://ipfs.io/)                                                     |

[^1]: Which, if you can believe it, was formalised nearly 40 years ago in 1982 and has only had fairly minor changes since then!
