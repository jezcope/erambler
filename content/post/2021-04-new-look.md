---
title: Time for a new look...
slug: new-look-2021
date: 2021-04-03T15:24:05+01:00
type: note
tags:
- Meta
- Design
---

I've decided to try switching this website back to using [Hugo][] to manage the content and generate the static HTML pages. I've been on the Python-based [Nikola][] for a few years now, but recently I've been finding it quite slow, and very confusing to understand how to do certain things. I used Hugo recently for the [GLAM Data Science Network website](https://glamdatasci.network) and found it had come on a lot since the last time I was using it, so I thought I'd give it another go, and redesign this site to be a bit more minimal at the same time.

The theme is still a work in progress so it'll probably look a bit rough around the edges for a while, but I think I'm happy enough to publish it now. When I get round to it I might publish some more detailed thoughts on the design.

[Hugo]: https://gohugo.io

[Nikola]: https://getnikola.com/
