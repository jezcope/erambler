---
title: "Comments are back"
description: "...powered by Matrix!"
slug: comments-are-back
date: 2021-06-29T20:58:40+01:00
type: post
tags:
- Meta
- Matrix
- Fediverse
---

I forgot to mention it at the time,
but I've added "normal" comments back to the site,
as you'll see below and on most other pages.
In place of the Disqus comments I had before
I'm now using [Cactus Comments][],
which is open source and self-hostable
(though I'm currently not doing that).
If you've read my previous post about [Matrix self-hosting][matrix],
you might be interested to know
that Cactus uses Matrix rooms for data storage and synchronisation
and I can moderate and reply to comments
directly from my Matrix client.

[Cactus Comments]: https://cactus.chat/
[matrix]: {{< relref "2021-03-matrix-self-hosting" >}}

I've still left the [webmention][] code in place too
so you can still comment that way
either on your own site or via social media.

[webmention]: {{< relref "2020-testing-webmentions.md" >}}
