---
title: Twitter archive
slug: twitter-archive
date: 2023-07-10T19:02:14+01:00
type: note
tags:
- Social Media
- Twitter
---

Just a quick note to say
that since I've deleted my Twitter account,
I've set up a [static archive of all my tweets](/twitter-archive/).
Yes, even those embarrassing first ones.

This was made possible by Darius Kazemi:
<https://tinysubversions.com/twitter-archive/make-your-own/>

You can still
[find me on Mastodon as @petrichor:digipres.club](https://digipres.club/@petrichor)
or via any of the other links at the bottom of the page.
