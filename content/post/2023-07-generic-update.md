---
title: "Vaguely generic update"
slug: vaguely-generic-update
date: 2023-07-20T17:17:22+01:00
type: post
tags:
- Meta
- Update
---

If it seems like I haven't posted here much lately
then you're right.
Here's a few bits and pieces I thought I'd share.

## Nix & NixOS

I've been learning a lot about [nix (the package manager) and NixOS (the Linux distribution)][NixOS] lately,
and enjoying the consistency and stability it brings.
The whole machine's setup and my user config
can all be specified in nixlang
and applied reproducibly and consistently on multiple computers.
The configuration is declarative:
instead of specifying the steps to configure the system
(install this package, modify that file)
I specify the desired state of the system
(these packages are available, these apps are configured like this)
and nix updates the actual state accordingly.
I can also specify isolated, reproducible environments for individual projects,
and deploy services to the cloud,
using the same tooling.
The language grammar is simple,
but the way it's interpreted takes a bit of getting used to
and I'm finding my appreciation of its elegance growing as I understand it more.
This website is now [built and deployed using nix](https://codeberg.org/jezcope/erambler/src/branch/production/nix)!

Cool NixOS stuff includes:

- [Home Manager](https://github.com/nix-community/home-manager): apply a declarative configuration to your user as well as your system
- [nix-doom-emacs](https://github.com/nix-community/nix-doom-emacs): use nix to configure and maintain your [Doom emacs](https://github.com/doomemacs/doomemacs) setup
- [Stylix](https://github.com/danth/stylix/): consistent theming for multiple different programs
- [deploy-rs](https://github.com/serokell/deploy-rs): easily configure remote servers using nix

[NixOS]: https://nixos.org


## Tabletop role-playing

I've been getting into tabletop role-playing games (TTRPGs)
at my local games café,
initially Dungeons & Dragons 5th edition
but increasingly [Cypher System][] games like [Numenera][].
Of course once I get into a thing
I get _really_ into the thing,
so not only am I playing these games
but increasingly acting as GM
to run games for others.
I attended [Collaborations Workshop 2023][CW23] earlier in the year,
and enjoyed running a game for some friends there one evening.

[Cypher System]: https://cypher-system.com/

[Numenera]: http://numenera.com/

[CW23]: https://software.ac.uk/cw23
