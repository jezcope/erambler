---
title: "Best practice, or ignoring the care label"
description: "In which I take a closer look at the label in my trousers, and then do my own thing."
slug: best-practice-or-ignoring-the-care-label
date: 2024-02-05T19:37:00+00:00
type: post
tags:
- Ablism
- Accessibility
- Best practice
- Good practice
---

I was hanging the laundry the other day, and ended up thinking about reasons why you might ignore those coded instructions on the care label of your clothing. I came up with quite a few...

<!--more-->

- The label isn't accessible to you (e.g. you're blind or partially sighted)
- Your needs don't match the assumptions of the manufacturer
- You habitually remove them because they itch
- You don't have the necessary equipment
- You don't understand the consequences
- You don't know what the symbols mean
- You can afford to buy more clothes
- Your clothes don't have them
- You don't know they're there
- You don't have the energy
- You don't have the time

There are certainly many more that I haven't thought of.

Anyway, it seemed like a good example of why "best practice" doesn't work:
it suggests that there is one "best" way of doing any given task
and that any other way of doing that task is necessarily inferior.
It seems like a minor niggle,
but I prefer "good practice" because it seems more appropriate
to have a collection of good (or even "good enough") practices
that you can choose from and combine depending on your context.
