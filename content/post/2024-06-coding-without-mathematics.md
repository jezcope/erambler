---
title: "On learning to code without mathematics"
summary: |
  In which I wonder why it should be that mathematical aptitude
  is considered a necessary prerequisite for learning to code
  when natural language aptitude seems a much better predictor
  of coding ability.
slug: coding-without-mathematics
date: 2024-06-03T22:06:48+01:00
type: post
tags:
- Learning
- Teaching
- Programming
- Mathematics
- Lisp
- Racket
updates:
  - date: 2024-06-05T13:41:57+0100
    text: |
      Rephrased to remove (unintended) implication that maths is taught badly
      (thanks [@TeaKayB@mathstodon.xyz](https://mathstodon.xyz/@TeaKayB/112563259415653746)
      for pointing this out and further discussion)
---

Why is it that so many beginner programming tutorials
assume that the learner is both
a) comfortable with maths; and
b) motivated to learn by seeing simple arithmetic?
Go look at your favourite tutorial
(I'll wait)
and I'll give you good odds it starts with some variation of
"look, you can use this as a calculator!"

<!--more-->

Seriously,
there must be so many people who would be great programmers
but who never get past the thought:

> "Why would I want to install and configure
> this whole programming environment
> just to see that 5×7 is 35?"

A *lot* of people are instantly put off when they see mathematics.
That's not a personal failure,
it's a natural consequence of the (inaccurate) way maths is seen in our culture,
as a binary thing where everything is questions
that have only one "right" answer
and a lot of wrong ones,
though it's actually a beautiful, creative and constantly evolving language.
There's a weird little duality,
often instilled in us before we even reach school,
whereby on the one hand
not being able to "do maths" is seen as a shameful thing
while on the other
it's far more acceptable to make a general statement like
"oh, I don't really do maths"
than to say
"oh, I don't really do reading".

That perceived double bind must put off loads of people
who would otherwise be very creative solvers of problems involving some maths,
and when we make it look like being "good at maths"
is a prerequisite of being able to program a computer
we put those same people off that too.
In any case I believe this is all based on a false premise:
maths is much more accessible than many have been led to believe,
and learning to code by manipulating text or images
has a lot of potential for demystifying mathematical concepts through familiarity.

_Aside: 
there is some evidence[^1] that natural language skill
is a better predictor of programming aptitude
than mathematical skill,
though I don't know whether that study has been replicated._

[^1]: Prat, C.S. et al. (2020)
      “Relating natural language aptitude to individual differences in learning programming languages,”
      Scientific reports, 10(1), p. 3817. Available at: https://doi.org/10.1038/s41598-020-60661-8.

Anyway,
that's why I really like the
[tutorial for the Racket programming language][tutorial]:
it focuses 100% on drawing pictures
and introduces programming concepts as ways of composing simple shapes
(like squares and circles)
into more complex images.

Building on this idea,
[Christine Lemmer-Webber][] has a tutorial for Digital Humanities folk
called ["Building a snowman with Racket"][]
which takes the learner through making a little picture of a snowman
using Racket's [slideshow module][].

I don't know about you,
but I think that's pretty cool
and I'd like to see more tutorials taking a similar approach.

{{< admonition "Further reading" link-light >}}
This post was prompted by:
- ["Lisp but beautiful; Lisp for everyone"](https://archive.fosdem.org/2022/schedule/event/lispforeveryone/),
  Christine & Morgan Lemmer-Webber's 2022 FOSDEM presentation about the Racket dialect of Lisp
- [Episode 49 of their podcast FOSS & Crafts](https://fossandcrafts.org/episodes/49-lisp-but-beautiful-lisp-for-everyone.html) (the audio from the above talk), which I listened to while driving down the A1 the other day
{{< /admonition >}}

[tutorial]: https://docs.racket-lang.org/quick/

[Christine Lemmer-Webber]: https://dustycloud.org/

["Building a snowman with Racket"]: https://dustycloud.org/misc/digital-humanities/Snowman.html

[slideshow module]: https://docs.racket-lang.org/slideshow/
