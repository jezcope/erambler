---
title: "On Donella Meadows and Systems Thinking"
summary: |
  In which I get a little bit overexcited about
  my fascination with complex systems and the way they fit together,
  thanks to Donella Meadows'_Thinking in Systems: A Primer_.
slug: donella-meadows-systems-thinking
date: 2024-06-18T19:08:39+01:00
type: post
tags:
- Systems Thinking
- Donella Meadows
- Environmentalism
- Degrowth
- Capitalism
---

This weekend I started reading
[Donella Meadows](https://en.wikipedia.org/wiki/Donella_Meadows)'
_[Thinking in Systems: A Primer](https://en.wikipedia.org/wiki/Thinking_In_Systems:_A_Primer)_
and I cannot overstate how profoundly glad I am
to have come across [Systems Thinking](https://en.wikipedia.org/wiki/Systems_thinking)
as a whole field of study.
It pulls together so many things that have interested me over the years,
and makes sense of a whole load of things I've observed about the world around me.

<!--more-->

I used to tell people that my abiding interest over time was the study of systems,
but stopped because most people assumed that meant _computer_ systems.
I meant complex systems in the broadest sense,
and I found it terribly frustrating
that people thought I would focus only on
the stereotypical white male nerd interest of "computers".

Obviously,
if you know me at all,
then assuming the unsaid "computer" in systems is pretty reasonable,
as it's where I spend most of my time.
Computers were the first systems I found
that I could have a significant level of control over.
They gave me a marketable skillset
and I fitted into the stereotype
so my interest in them was encouraged.

But I've always been just as interested in computers and software
for the part they play in wider systems,
especially those involving people.
Maybe that's partly why I've never been tempted
to go for higher paid work in the software industry
where I would be expected to have that narrow focus
and only care about my team's sub-sub-subsystem.
I guess as an undiagnosed autistic kid,
developing a special interest in the interactions and relationships between people was something that enabled me to go through the motions and fit in,
which undoubtedly kept me safe,
though probably at the expense of my mental health.

In any case,
I am where I am now and thankfully have enough time and energy to explore this.
Meadows' work resonates with me especially
because of the role she played in bringing
a systems perspective to environmental and societal issues.
She was the main author of the 1972 report,
_[The Limits to Growth](https://en.wikipedia.org/wiki/The_Limits_to_Growth)_,
which predicted that a business-as-usual approach to economic and population growth on a finite planet would inevitably lead to a collapse.
Largely ignored by mainstream policymakers at the time,
those predictions are looking increasingly prescient.

_N.B. This post is an extended version of
[this Mastodon thread.](https://digipres.club/@petrichor/112626251686797022)_
