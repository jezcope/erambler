---
title: "On being told to do more with less"
summary: |
  In which I question whether "efficiency" is all it's cracked up to be.
slug: more-with-less
date: 2024-08-08T20:54:48+01:00
type: post
tags:
- Efficiency
- Leadership
- Work
- Capitalism
---

Why do we celebrate "efficiency" so much?

Cuts to public services are vaunted as "efficiency savings". Free markets (and by extension capitalism though they're not technically the same thing) are touted as the most "efficient" way to distribute resources. New technologies are sold on promises of more efficient working practices.

But we rarely question who benefits from these supposed efficiencies, and who pays the cost.

One example I've run into time and time again will be familiar to anyone who's worked in a large organisation: replacing experienced specialist staff (often on finance or HR, but anything seen as a "support service" is at risk) with an IT system that enables, "self-service".

Usually this is celebrated as a resounding success, since it's generally followed by a reduction in headcount with no cost other than the cost of the system, which is both lower and reduces your risk because you don't to have to make redundancy payouts when you get rid of a service provider. The thing these assessments never seem to account for is that the work doesn't disappear: it simply moves somewhere its cost isn't so visible on a balance sheet.

Suppose my employer has introduced a Revolutionary New System™ to let me submit my own expense claims via the web. I do this a handful of times a year, so every time I have to do it I need to:

1.  Find the expenses policy
2.  Check it really is the latest version
3.  Figure out what limits apply and which categories each of my expenses fall into
4.  Remember how to put all of that into the counter-intuitive self-service system
5.  Apply the right accounting codes
6.  Find a working scanner and scan the receipts
7.  Upload those and finally hit submit

If I've made a mistake, it'll be sent back to me to try and figure out what I did wrong, after which I'll have to repeat the process.

In contrast, the finance assistant replaced by that self-service system did that (and a range of other tasks) on a daily or weekly basis. They knew the policy, they knew the edge cases, they knew the mistakes to look for and how to explain to me how to fix them. In the time it took me to submit one very average expense claim, they could process several based only on the scanned receipts they received. They were a specialist and good at their job. I'm great at my job but not so great at doing theirs too. Multiply that across every employee who travels for work, or otherwise pays out of pocket for things that need to be claimed back from the employer, and for every person you've made redundant, you've reduced your organisation's capacity to get things done by much more by pushing specialist work onto non-specialists.

The balance sheet tells a different story though: you've (apparently) reduced your pay bill and all that work is (apparently) still being done, so everyone wins, must be an efficiency saving. The things those making these assertions never seem to ask is this: what work is *not* being done for this to continue being done by fewer people?

Alternatively, maybe everyone ups their pace, picks up the extra work and manages to continue as before, eating into any spare capacity they might have had. They're rushing through things that they would've checked before, so mistakes start to slip through. When the mistakes come to light, they need to be fixed, which is more work that must be done, further eating into that spare capacity. The end of that road is catastrophe for the employer and burnout for the employee, but it will have an impact long before then.

Consider [this example of a corporate invoicing scam](https://social.pixie.town/@joepie91/112903335186204714), where a fake invoice is sent to a company's accounts department in such a way to manipulate someone into paying the scammer as though they are a real supplier. As \@joepie90 notes in that thread, and like so many others, this type of scam relies on cranking up the time pressure on the recipient, giving them less time to make an accurate assessment of how they should act. Who's more likely to get that wrong? Someone in a team of four under pressure to pick up a workload previously carried by six.

Reducing spare capacity also hits resilience. We saw this writ large with the pandemic: the recent [UK Covid-19 Inquiry Report](https://www.gov.uk/government/publications/uk-covid-19-inquiry-resilience-and-preparedness-module-1-report) described "public services ... running close to, if not beyond capacity" after years of austerity. The [National Cyber Security Centre has also hinted at this](https://www.ncsc.gov.uk/blog-post/legislation-help-counter-cyber-threat-cni), saying that our resilience has not developed "at the pace necessary to match our adversaries".

None of the issues I've just described are easy to capture on a spreadsheet. So what looks like a success on paper --- a consequence-free reduction in staff costs --- is in fact nothing of the sort. The costs are still there, and probably greater than before, but they've been shuffled around in a way that makes them hard to see.

It's like that [optical illusion where a bar of chocolate is sliced up into uneven shaped pieces](https://en.wikipedia.org/wiki/Missing_square_puzzle), which are then apparently rearranged to make a chocolate bar the same size as the original but with one square left over. It looks like you've made free chocolate from thin air, but the space where that chocolate used to be is long- and thin-enough that your eye assumes the pieces just haven't been pushed all the way together.

In other words, these amazing efficiency savings are a sleight of hand meant to hide a reduction in capacity by spreading it so thinly no one notices. In the short term we're all fooled into doing a tiny bit more work, and the person at the top gets a nice bonus for their financial prudence. But do it over and over again and people start to notice they're working harder than ever while getting less and less done. You can't keep that up long without burning out large portions of your workforce.

You don't get infinite chocolate for free.
