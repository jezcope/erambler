---
title: Advent of Code 2017
---

This series of posts documents my solutions to the programming problems in the [Advent of Code 2017][AoC2017] challenge, completed in a variety of languages.

[AoC2017]: https://adventofcode.com/2017
