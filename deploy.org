#+title: Site configuration
#+property: header-args :noweb yes :comments link

* Deployment

** Continuous integration
:PROPERTIES:
:ID:       1e6916f2-07d7-4ea1-afba-5c2b2b8ec0f9
:END:

The build step is very simple, and runs ~hugo~ to build the site, which is then stored in the ~public~ folder.

#+begin_src yaml :noweb-ref ci/build-step
  name: build
  <<ci/image>>
  commands:
    - hugo
  when:
    event: push
#+end_src

The preview step pushes the built site to Netlify using their own CLI tool, making it accessible via a randomised URL so it can be previewed.

#+begin_src yaml :noweb-ref ci/preview-step
  name: deploy-preview
  commands:
    - netlify deploy --site erambler.co.uk --dir public
  <<ci/image>>
  <<netlify/deploy-secrets>>
#+end_src

This step should be executed on any branch /except/ ~production~.

#+begin_src yaml :noweb-ref ci/preview-step
  when:
    - event: push
      branch:
        exclude: production
#+end_src

The production deployment step is almost identical, but adds the ~--prod~ flag to the command-line.

#+begin_src yaml :noweb-ref ci/deploy-step
  name: deploy-production
  commands:
    - netlify deploy --site erambler.co.uk --dir public --prod
  <<ci/image>>
  <<netlify/deploy-secrets>>
#+end_src

This step should be executed /only/ on the ~production~ branch.

#+begin_src yaml :noweb-ref ci/deploy-step
  when:
    - event: push
      branch: production
#+end_src

Both the deployment steps need an authentication token which is configured using the Woodpecker CI secrets UI, and need to explicitly declare their dependency on it. It is brought in as an environment variable.

#+begin_src yaml :noweb-ref netlify/deploy-secrets
  secrets: [ netlify_auth_token ]
#+end_src

These are combined into the final pipeline:

#+begin_src yaml :tangle .woodpecker.yaml
  steps:
    -
      <<ci/build-step>>
    -
      <<ci/preview-step>>
    -
      <<ci/deploy-step>>
#+end_src

** Build image
:PROPERTIES:
:ID:       aeaf7659-4484-493c-9da4-f4c8e283404d
:END:

This very simple ~Dockerfile~ creates an image including both ~hugo~ (to build the site) and ~netlify-cli~ to deploy it.

#+begin_src dockerfile :tangle Dockerfile
  FROM node:lts-alpine
  ENV NODE_ENV=production
  RUN apk add --no-cache hugo \
      && npm install -g netlify-cli\
      && npm cache clean --force
  RUN wget -P /tmp https://github.com/sass/dart-sass/releases/download/1.77.2/dart-sass-1.77.2-linux-x64-musl.tar.gz \
      && tar -C /usr/local/bin --strip-components=1 \
           -xf /tmp/dart-sass-1.77.2-linux-x64-musl.tar.gz \
      && rm /tmp/dart-sass-1.77.2-linux-x64-musl.tar.gz
#+end_src

This can be built and pushed to [[https://hub.docker.com][DockerHub]]:

#+begin_src shell
  sudo docker build --tag jezcope/erambler .
  sudo docker push jezcope/erambler
#+end_src

Finally, reference the image tag in each step in the [[id:1e6916f2-07d7-4ea1-afba-5c2b2b8ec0f9][CI pipeline]]:

#+name: ci/image
#+begin_src yaml
  image: jezcope/erambler:2024-05-23
#+end_src
