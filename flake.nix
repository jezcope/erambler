{
  description = "Content and server config for erambler.co.uk";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.twitter-archive = {
    url =
      "https://codeberg.org/api/v1/repos/jezcope/twitter-archive/archive/main.tar.gz";
    flake = false;
  };

  outputs = { self, nixpkgs, flake-utils, twitter-archive }:
    {
      overlay = self: super:
        let
          pkgs = nixpkgs.legacyPackages.${super.system};
          buildIosevka = pkgs.callPackage ./nix/iosevka.nix;
        in rec {
          iosevka = buildIosevka { set = "mnemosyne"; };
          iosevka-aile = buildIosevka { set = "aile-mnemosyne"; };
          iosevka-etoile = buildIosevka { set = "etoile-mnemosyne"; };

          fonts = pkgs.symlinkJoin {
            name = "mnemosyne-fonts";
            paths = [ iosevka iosevka-aile iosevka-etoile ];
          };

          erambler =
            pkgs.callPackage ./nix/build.nix { inherit fonts twitter-archive; };

          inherit twitter-archive;
        };

      nixosModule = import ./nix/server.nix { inherit self; };
    } // flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        };
      in {
        packages = {
          inherit (pkgs) erambler iosevka iosevka-aile iosevka-etoile fonts;
          default = pkgs.erambler;
        };

        devShell = pkgs.callPackage ./nix/shell.nix { };
      });
}
