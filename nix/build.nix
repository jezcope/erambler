{ pkgs, fonts, lndir, twitter-archive, ... }:

with pkgs;
stdenv.mkDerivation {
  name = "erambler-html";
  src = ../.;
  nativeBuildInputs = [ hugo fonts twitter-archive ];
  buildPhase = "hugo";
  installPhase = ''
    mkdir $out
    cp -R public/* public/.well-known "$out"
    ln -s ${twitter-archive} "$out/twitter-archive"
  '';
}
