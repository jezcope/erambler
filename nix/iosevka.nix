{ stdenv, lib, nodejs, nodePackages, remarshal, ttfautohint-nox
, privateBuildPlan ? builtins.readFile ./private-build-plans.toml
, extraParameters ? null, set ? null }:

assert (privateBuildPlan != null) -> set != null;
assert (extraParameters != null) -> set != null;

let
  # We don't know the attribute name for the Iosevka package as it
  # changes not when our update script is run (which in turn updates
  # node-packages.json, but when node-packages/generate.sh is run
  # (which updates node-packages.nix).
  #
  # Doing it this way ensures that the package can always be built,
  # although possibly an older version than ioseva-bin.
  nodeIosevka =
    (lib.findSingle (drv: drv ? packageName && drv.packageName == "iosevka")
      (throw "no 'iosevka' package found in nodePackages")
      (throw "multiple 'iosevka' packages found in nodePackages")
      (lib.attrValues nodePackages)).override (drv: { dontNpmInstall = true; });
in stdenv.mkDerivation rec {
  pname = if set != null then "iosevka-${set}" else "iosevka";
  inherit (nodeIosevka) version src;

  nativeBuildInputs = [ nodejs nodeIosevka remarshal ttfautohint-nox ];

  buildPlan = if builtins.isAttrs privateBuildPlan then
    builtins.toJSON { buildPlans.${pname} = privateBuildPlan; }
  else
    privateBuildPlan;

  inherit extraParameters;
  passAsFile = [ "buildPlan" "extraParameters" ];

  configurePhase = ''
    runHook preConfigure
    ${lib.optionalString (builtins.isAttrs privateBuildPlan) ''
      remarshal -i "$buildPlanPath" -o private-build-plans.toml -if json -of toml
    ''}
    ${lib.optionalString (builtins.isString privateBuildPlan) ''
      cp "$buildPlanPath" private-build-plans.toml
    ''}
    ${lib.optionalString (extraParameters != null) ''
      echo -e "\n" >> params/parameters.toml
      cat "$extraParametersPath" >> params/parameters.toml
    ''}
    ln -s ${nodeIosevka}/lib/node_modules/iosevka/node_modules .
    runHook postConfigure
  '';

  buildPhase = ''
    runHook preBuild
    npm run build --no-update-notifier -- --jCmd=$NIX_BUILD_CORES contents::$pname >/dev/null
    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall
    install -d "$out/ttf"
    install -d "$out/woff2"
    install "dist/$pname/ttf"/* "$out/ttf"
    install "dist/$pname/woff2"/* "$out/woff2"
    install "dist/$pname/$pname.css" "$out"
    runHook postInstall
  '';

  enableParallelBuilding = true;

  passthru = { updateScript = ./update-default.sh; };

  meta = with lib; {
    homepage = "https://be5invis.github.io/Iosevka";
    downloadPage = "https://github.com/be5invis/Iosevka/releases";
    description = ''
      Slender monospace sans-serif and slab-serif typeface inspired by Pragmata
      Pro, M+ and PF DIN Mono, designed to be the ideal font for programming.
    '';
    license = licenses.ofl;
    platforms = platforms.all;
    maintainers = with maintainers; [
      cstrahan
      jfrankenau
      ttuegel
      babariviere
      rileyinman
      AluisioASG
    ];
  };
}
