{ self }:
{ config, pkgs, ... }:

{
  services.nginx = {
    enable = true;

    virtualHosts."erambler.co.uk" = {
      enableACME = true;
      forceSSL = true;

      root = self.packages.${pkgs.system}.erambler;
      extraConfig = ''
        rewrite ^/.well-known/(host-meta|webfinger).* https://fed.brid.gy$request_uri redirect;
      '';
      locations = {
        "~ ^/(feed|rss)(\\.xml|/)".return = "301 /index.xml";
        "~ ^/tags/([a-zA-Z-]+)\\.xml".return = "301 /tags/$1/index.xml";
        "~ ^/20(0\\d|1[0-5])/\\d\\d/\\d//d/([^\\s]*)".return = "301 /blog/$2";

        "/index.xml".extraConfig = ''
          add_header Link '<https://erambler.co.uk/rss.xml>; rel="self"';
        '';
      };
    };
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];
}
