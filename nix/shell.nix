{
  pkgs ? import <nixpkgs> { },
}:

with pkgs;
mkShell {
  buildInputs = [
    (python3.withPackages (
      py: with py; [
        python
        invoke
        rich
        requests
        ruamel_yaml
        sh
      ]
    ))
    yarn
    hugo
    dart-sass
    simple-http-server
  ];
}
