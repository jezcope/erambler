from invoke import task
from pathlib import Path
import sh

DEPLOY_DIR = Path(__file__).parent / "public"
REMOTE_DIR = "jemisin.petrichor.me:/srv/www/erambler.co.uk/"


@task
def deploy(c, clean=False):
    if clean:
        print("clean and rebuild...")
        sh.rm(DEPLOY_DIR, recursive=True, force=True)
        sh.hugo()
        print("done")

    print("push via rsync...")
    sh.rsync(
        f"{DEPLOY_DIR}/",
        REMOTE_DIR,
        checksum=True,
        archive=True,
        verbose=True,
        delete=True,
        _fg=True,
    )
    print("done")
