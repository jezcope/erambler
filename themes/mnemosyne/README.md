# Mnemosyne: a minimal Hugo theme

This is the theme that I use for [my blog, eRambler](https://erambler.co.uk).

## Installation

In your Hugo site `themes` directory, run:

```
git clone https://tildegit.org/petrichor/theme-mnemosyne-hugo.git
```

Next, open `config.toml` in the base of the Hugo site and ensure the theme option is set to `mnemosyne`.

```
theme = "mnemosyne"
```

For more information read the official [quick start guide](https://gohugo.io/getting-started/quick-start/) of Hugo.

## Contributing

I don't really have time to test & accept contributions, but feel free to fork this theme and do your own thing with it.

## License

This theme is released under the [MIT license](https://tildegit.org/petrichor/theme-mnemosyne-hugo/src/branch/main/LICENSE).

## Acknowledgements

This theme is based on: [Blank][], a starter [Hugo](https://gohugo.io/) theme for developers. Use it to make your own theme.

[Blank]: https://themes.gohugo.io/theme/blank/
